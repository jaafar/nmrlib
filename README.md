# NMRLib

NMRLib is a Python library that provides various tools for NMR simulations.

## Installation

Use the package manager [pip](https://pip.pypa.io/en/stable/)
to install NMRLib.

```bash
pip install git+https://gitlab.com/jaafar/nmrlib.git#egg=nmrlib
```
## Usage
<!--
```python
import foobar

foobar.pluralize('word') # returns 'words'
foobar.pluralize('goose') # returns 'geese'
foobar.singularize('phenomena') # returns 'phenomenon'
```
-->
## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://gitlab.com/jaafar/nmrlib/-/blob/ae172824f073d63f55196a9abc87af465bbc1b42/LICENSE)
