import unittest
from nqr.core import Nqr, NqrError
from mendeleev import element


class TestNqr(unittest.TestCase):
    def test_atom_input(self):
        # Test bad inputs
        self.assertRaises(TypeError, Nqr, 'TaP', 12)
        self.assertRaises(TypeError, Nqr, 'TaP')
        self.assertRaises(TypeError, Nqr, 'TaP', 181)

        # Test bad elements (must be 'Ta-181')
        self.assertRaises(ValueError, Nqr, 'TaP', 'La')
        self.assertRaises(ValueError, Nqr, 'TaP', '181Ta')
        self.assertRaises(ValueError, Nqr, 'TaP', '181-Ta')

        # Test bad mass numbers(must be 'Ta-181')
        self.assertRaises(ValueError, Nqr, 'TaP', 'Ta')
        self.assertRaises(ValueError, Nqr, 'TaP', 'Ta-')
        self.assertRaises(ValueError, Nqr, 'TaP', '-')
        self.assertRaises(ValueError, Nqr, 'TaP', 'Ta-s')

        # Test wrong isotope
        self.assertRaises(NqrError, Nqr, 'TaP', 'Ta-12')


    def test_values(self):
        TaP_nqr = Nqr('TaP', 'Ta-181')
        self.assertEqual(TaP_nqr.atom.name, 'Tantalum')
        self.assertEqual(TaP_nqr.atom.symbol, 'Ta')
        self.assertEqual(TaP_nqr.mass_number, 181)
        self.assertEqual(TaP_nqr.Q, 3.17e-28)
