#!/usr/bin/env python3
from fractions import Fraction
import os.path as op

import numpy as np
import numpy.linalg as la
import pandas as pd
from scipy.constants import e, h, mega, femto, milli
from mendeleev import element, Element
from uncertainties import ufloat, ufloat_fromstr

from nmrlib import ROOT
import nmrlib.quantum as quant
BARN = 1e-28


def get_isotope(atom, mass_number):
    for i, x in enumerate(element(atom).isotopes):
        if x.mass_number == mass_number:
            return x


class Nqr:
    def __init__(self, formula=None, atom=None,
                 Qcc=None, eta=None, efg=None):
        self.formula = formula

        if type(atom) != str:
            raise TypeError("'atom' must be str in this format:"
                             + " '<atom>-<mass number>'")
        atom_mass_num = atom.split('-')

        if type(atom_mass_num) != list or len(atom_mass_num) != 2:
            raise ValueError("'atom' must be str in this format:"
                             + " '<atom>-<mass number>'")
        else:
            try:
                _atom = int(atom_mass_num[0])
            except:
                _atom = atom_mass_num[0]
            try:
                _mass_num = int(atom_mass_num[1])
            except:
                raise ValueError(f"'{atom}' is missing mass number")
        self.mass_number = _mass_num

        # Handle atom input
        try:
            self.atom = element(_atom)
        except:
            raise TypeError(f"'{atom}' not a valid mendeleev.element() input")

        # Handle mass number input
        _isotope = get_isotope(self.atom, _mass_num)
        if _isotope == None:
            raise NqrError(f'{self.atom.name} has no isotope'
                           + f' with mass number {_mass_num}')
        self.isotope = _isotope
        self.spin = self.isotope.spin
        self.Q = self.isotope.quadrupole_moment*1e-28

#        self.Qcc = Qcc*mega if Qcc != None else None
#        self.eta = eta if eta != None else None
#        self.freqs = None

#        self.site = self.__class__._get_isotope(site, mass_num)
#        self.efg = None
#        #self.Q = Q if Q != None else get_Q(site)[tuple([site, self.Z])]
#        #self.nuQ = nuQ
#        #self.efg = efg

    def efg_from_Qcc(self):
        Vzz = self.Qcc*h/(e*self.Q)
        if self.eta == None:
            return Efg(self.mat, self.site, Vzz=Vzz)
        else:
            Vxx = Vzz*(self.eta - 1)/2
            Vyy = -Vxx - Vzz
            return Efg(self.mat, self.site, V=[Vxx, Vyy, Vzz])


    @classmethod
    def from_freqs(cls, mat, site, isotope, spin, freqs):
        components = np.array([tensor[0, 0], tensor[1,1], tensor[2,2]])
        sort_indices = np.argsort(np.abs(components))
        V = components[sort_indices]

        return cls(mat, site, isotope, spin)


    def get_H(self, eta=None):
        """
        Electric quadrupolar Hamiltonian,
        normalized with respect to 3*e*V_zz*Q/(4*I*(2*I-1)*h) [Suits].

        Args:
            I (float): spin
            eta (np.ndarray): 1D range of values from 0 to 1

        Returns:
            np.ndarray: Array of Hamiltonians, each corresponding
            to a single eta value.
        """

        if eta == None:
            eta = np.array(self.eta)
        else:
            eta = np.array(eta)
        eta = eta.reshape(eta.size, 1, 1) # Reshape to permit broadcasting
        I = self.I
        Ix, Iy, Iz, Ip, Im = quant.get_spin_matrices(I)
        identity = np.identity(int(2*I + 1))
        factor = e*self.Q/(4*I*(2*I - 1))
        H = h/3*(3*Iz@Iz - I*(I+1)*identity + eta*(Ix@Ix - np.real(Iy@Iy)))

        return H


    def __str__(self):
        out = 'material: {}\n'.format(self.formula)
        out += f'site: {self.element.symbol}-{self.isotope.mass_number}\n'
        out += 'spin: {}\n'.format(Fraction(self.spin))
        out += 'quadrupole moment: {} mb\n'.format(self.Q)
        out += 'quadrupole coupling constant: {} MHz\n'.format(self.Qcc)\
               if self.Qcc != None else ''
        out += 'eta: {}\n'.format(self.eta) if self.eta != None else ''
        out += 'frequencies: {} MHz\n'.format(self.freqs)\
               if self.freqs != None else ''

        return out


class Efg:
    """Electric field gradient object

    Args (optional):
        mat (str): chemical formula of material
        site (str): atomic symbol of nuclear site where EFG is measured
        V (list): principle axes components of EFG following standard
        convention (|Vzz| > |Vyy| > |Vxx|)
        (can be list of lists for multiple EFGs)
        factor (float): default units for input are `factor`*V/m^2;
        `factor` is 1e21 by default.

    Usage:
        >>> efg = Efg(V=[[1.1, 1.0, 1.3], [2.3, 2.5, 2.4], [4.2, 4.5, 4.1])
        >>> efg
        Efg(None, None,Vxx=[1.1e+21 2.3e+21 4.2e+21], Vxx=[1.0e+21 2.5e+21\
4.5e+21], Vzz=[1.3e+21 2.4e+21 4.1e+21])
        >>> efg.Vyy
        array([1.0e+21, 2.5e+21, 4.5e+21])

        >>> efg2 = Efg('TaP, 'Ta', V=[-0.865, -2.139, 3.004])
        >>> efg2
        Efg(TaP, Ta, Vxx=-0.865, Vxx=-2.139, Vzz=3.004) 1e21 V/m^2
        >>> efg2.eta
        0.4241011984021304
    """


    def __init__(self, mat=None, site=None, V=None,
                 pac_tensor=None, gac_tensor=None):
        self.mat = mat
        self.site = site
        self._pac_tensor = pac_tensor
        self._gac_tensor = gac_tensor
        self.V = np.array(V) if type(V) != None else None
        self.Vxx = self.V[..., 0] if type(V) != None else None
        self.Vyy = self.V[..., 1] if type(V) != None else None
        self.Vzz = self.V[..., 2] if type(V) != None else None


    @property
    def eta(self):
        return (self.Vxx - self.Vyy)/self.Vzz


    @property
    def pac_tensor(self):
        return self._pac_tensor


    @pac_tensor.setter
    def pac_tensor(self, tensor):
        self._pac_tensor = tensor


    @classmethod
    def from_pac_tensor(cls, mat=None, site=None,
                        pac_t=None,
                        gac_t=None):
        components = np.diag(pac_t)
        sort_indices = np.argsort(np.abs(components))
        V = components[sort_indices]

        return cls(mat, site, V=V, pac_tensor=pac_t, gac_tensor=gac_t)


    @property
    def gac_tensor(self):
        return self._gac_tensor


    @gac_tensor.setter
    def gac_tensor(self, tensor):
        self._gac_tensor = tensor


    @classmethod
    def from_gac_tensor(cls, mat=None, site=None, tensor=None):
        eig_vals = la.eigvals(tensor)
        pac_t = np.diag(eig_vals)

        return cls.from_pac_tensor(mat, site, pac_t, tensor)



    def __neg__(self, other):
        return self.__class__(self.mat, self.site, -1*self.V,
                              self.pac_tensor, self.gac_tensor)


    def __mul__(self, other):
        return self.__class__(self.mat, self.site, other*self.V,
                              self.pac_tensor, self.gac_tensor)


    def __rmul__(self, other):
        return self*other


    def __truediv__(self, other):
        return self.__class__(self.mat, self.site, self.V/other,
                              self.pac_tensor, self.gac_tensor)


    def __repr__(self):
        if np.any(self.V == None):
            Vxx = None
            Vyy = None
            Vzz = self.Vzz
        else:
            Vxx = self.Vxx
            Vyy = self.Vyy
            Vzz = self.Vzz

        return f'Efg({self.mat}, {self.site}, \
Vxx={Vxx}, Vyy={Vyy}, Vzz={Vzz})'


    def __str__(self):
        return f'{self.V}'


def get_norm_H(I, eta):
    """
    Electric quadrupolar Hamiltonian,
    normalized with respect to 3*e*V_zz*Q/(4*I*(2*I-1)*h) [Suits].

    Args:
        I (float): spin
        eta (np.ndarray): 1D range of values from 0 to 1

    Returns:
        np.ndarray: Array of Hamiltonians, each corresponding
        to a single eta value.
    """

    eta = np.array(eta)
    eta = eta.reshape(eta.size, 1, 1) # Reshape to permit broadcasting
    Ix, Iy, Iz, _, _ = quant.get_spin_matrices(I)
    identity = np.identity(int(2*I + 1))
    #H = h/3*(3*Iz@Iz - I*(I+1)*identity + eta*(Ix@Ix - np.real(Iy@Iy)))
    H = h/3*(3*Iz@Iz - I*(I+1)*identity + eta*(Ix@Ix - Iy@Iy))

    return np.squeeze(H)

def get_norm_H3D(I, eta):
    """
    Electric quadrupolar Hamiltonian,
    normalized with respect to 3*e*V_zz*Q/(4*I*(2*I-1)*h) [Suits].

    Args:
        I (float): spin
        eta (np.ndarray): 1D range of values from 0 to 1

    Returns:
        np.ndarray: Array of Hamiltonians, each corresponding
        to a single eta value.
    """

    eta = np.array(eta)
    eta = eta.reshape(eta.shape[0], eta.shape[1], 1, 1) # Reshape to permit broadcasting
    Ix, Iy, Iz, _, _ = quant.get_spin_matrices(I)
    identity = np.identity(int(2*I + 1))
    #H = h/3*(3*Iz@Iz - I*(I+1)*identity + eta*(Ix@Ix - np.real(Iy@Iy)))
    H = h/3*(3*Iz@Iz - I*(I+1)*identity + eta*(Ix@Ix - Iy@Iy))

    return np.squeeze(H)

def get_hamiltonian(I, Q, V):
    Vxx, Vyy, Vzz = V[0], V[1], V[2]
    Ix, Iy, Iz = quant.get_spin_matrices(I)
    identity = np.identity(len(Iz))
    Ix2 = np.matmul(Ix, Ix)
    Iy2 = np.real(np.matmul(Iy, Iy))
    Iz2 = np.matmul(Iz, Iz)
    H = e*Q/(4*I*(2*I-1))*(Vzz*(3*Iz2 - I*(I+1)*identity) + (Vxx - Vyy)*(Ix2 - Iy2))

    return H

def get_freqs(H):
    E, _ = la.eig(H)
    E_diff = np.sort(np.diff(np.sort(np.real(E))))
    f = E_diff/h

    return f[f > 1]


def get_Q(atom, mass_number):
    """
    Get nuclear quadrupole moment value in m^2,
    from Pekka Pyykkö year 2017 data
    [https://doi.org/10.1080/00268976.2018.1426131].

    Args:
        atom (str): atomic symbol
        mass_number (int): mass number of desired isotope

    Returns:
        tuple of quadrupole moment and uncertainty
    """
    quad_mom_path = op.join(ROOT, 'resources', 'quadmom')
    csv = op.join(quad_mom_path, 'quadmom_2017.csv')
    df = pd.read_csv(csv, index_col=['nucleus', 'mass_num'],
                     dtype={'q_mb':object, 'uncert':object})

    q_df = df.loc[(atom, mass_number)][['q_mb', 'uncert']]
    q = q_df['q_mb']
    uncert = q_df['uncert']

    if pd.isnull(uncert):
        return ufloat_fromstr(q)*milli*BARN
    else:
        return ufloat_fromstr(f'{q}+/-{uncert}')*milli*BARN


def get_Q2(*args):
    """
    Get nuclear quadrupole moment value in mb (1e-28 m^2),
    from Pekka Pyykkö year 2017 data
    [https://doi.org/10.1080/00268976.2018.1426131].

    Args:
        Give any number of nuclei and associated isotopes.
        Nucleus must be string, isotopes can be int or list of ints or None.
        If None, all isotopes are returned.
        E.g,: get_Q('N', 14) and get_Q('N') return the same Qs.
              get_Q('N', 'Cl', [35, 37]) and get_Q('N', 'Cl') return same Qs
              get_Q('N', 'Cl') returns all Qs for all isotopes

    Returns:
        Dictionary with keys in the form of (nucleus, isotope)
    """
    quad_mom_path = op.join(ROOT, 'references', 'quad_mom')
    csv = op.join(quad_mom_path, 'quad_mom_2017.csv')
    df = pd.read_csv(csv, index_col=['nucleus', 'isotope'])

    # build list of indices
    indices = []
    for i, arg in enumerate(args):
        if type(arg) == int:
            continue
        if arg != args[-1]:
            if type(args[i+1]) != str:
                isos = args[i+1]
                if type(isos) != list:
                    isos = [isos]
                for x in isos:
                    indices.append(tuple([args[i], x]))
            else:
                indices.append(args[i])
        else:
            indices.append(args[i])

    Qs = {}
    for i in indices:
        d = df.loc[[i], :]['q'].to_dict()
        Qs.update(d)

    return Qs



if __name__ == '__main__':
    pass
#    print('Ac-227: ', get_Q('Ac', 227))
#    print('I-129: ', get_Q('I', 129))
#    print('Li-7: ', get_Q('Li', 7))
#    _clean_Q_csv()
#    Qs = get_Q('Ta')
#    print(Qs)
#    import timeit
#    mysetup = '''
#from __main__ import get_norm_H
#import numpy as np
#I = 7/2
#eta = np.linspace(0, 1, 1000000)'''
#    mycode2 = '''
#for i in np.linspace(0, 1, 1000000):
#    get_norm_H(I, i)'''
#    mycode1 = "get_norm_H(I, eta)"
#    time1 = timeit.timeit(stmt=mycode1, setup=mysetup, number=1)
#    time2 = timeit.timeit(stmt=mycode2, setup=mysetup, number=1)
#    print(time1, time2, np.abs(time2-time1)/time2)
