#!/usr/bin/env python3
import os.path as op

import pandas as pd

def clean_quadmom_csv(raw_name='t0002-10.1080_00268976.2018.1426131'):
    """
    Clean quadrupole moment CSV.

    This function cleans the quadrupole moment data
    from the Pekka Pyykkö year 2017 data
    [https://doi.org/10.1080/00268976.2018.1426131].

    The CSV from Table 2 is used.
    The default filename from the journal is used.
    The file should be placed in `resources/quadmom`,
    which is also where the cleaned output is placed.
    """
    root_path = op.join(op.dirname(__file__), '../')
    quadmom_path = op.join(root_path, 'resources', 'quadmom')
    csv_in_path = op.join(quadmom_path, raw_name + '.csv')
    csv_out_path = op.join(quadmom_path, 'quadmom_2017.csv')

    raw = pd.read_csv(csv_in_path)
    df = [None]*3
    df[0] = raw.iloc[:, 0:3]
    df[1] = raw.iloc[:, 3:6]
    df[2] = raw.iloc[:, 6:]
    df[1].columns = df[0].columns
    df[2].columns = df[0].columns
    df = pd.concat(df)

    # Remove empty rows
    df = df[df['Nucleus'].str.strip().astype(bool)]

    # Split nucleus column into seperate nucleus, istope, and excited columns
    new_cols = df['Nucleus'].str.extract(r'([a-zA-Z]+)-([\d]+)(\*)*')
    df['Nucleus'] = new_cols[0]
    df['isotope'] = new_cols[1]
    df['excited'] = new_cols[2].replace(r'\*', True, regex=True)

    # Create "new values" column
    new_cols = df['Q'].str.split('•', n=1).str
    df['Q'], df['new'] = new_cols[0], new_cols[1]
    df.fillna(value=False, inplace=True)
    df['new'].replace(r'\s*', True, regex=True, inplace=True)

    # Separate value and uncertainies into 2 columns
    df['uncert'] = df['Q'].str.extract(r'\((\d*)\)')
    df['Q'] = df['Q'].str.extract(r'([−\.\d]+)(?:\(\d*\))*')
    df['Q'].replace(r'−', '-', regex=True, inplace=True)

    # Add name to 'methods' column
    df = df.rename(columns={df.columns[2]: 'methods'})

    # Reorder columns and make lowercase
    cols = ['Nucleus', 'isotope', 'Q', 'uncert', 'new', 'excited', 'methods']
    df = df[cols]
    df = df.rename(str.lower, axis='columns')

    # Rename some column names
    df = df.rename(columns={df.columns[1]: 'mass_num'})
    df = df.rename(columns={df.columns[2]: 'q_mb'})

    # Fix data types
    #types_map = {'mass_num': 'int64', 'q_mb': 'float64', 'uncert': 'float64'}
    types_map = {'mass_num': 'int64', 'q_mb':object, 'uncert':object}
    df = df.astype(types_map)

    # Add data not included in Pyykko dataset
    new_data = pd.DataFrame([['I', 129, '-488', '8', '', '', '']],
                            columns=df.columns)
    df = df.append(new_data)

    # Reset index column
    df = df.reset_index(drop=True)

    df.to_csv(csv_out_path)

    return None

if __name__ == '__main__':
    clean_quadmom_csv()
