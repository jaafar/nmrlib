#!/usr/bin/env python3
import numpy as np
import nmrlib.core

def get_spin_matrices(I):
    """
    Quantum spin angular momentum operators.

    Args:
        I (float): spin

    Returns:
        Returns Ix, Iy, Iz, I+, I- spin operators
        as 2D arrays
    """
    n = int(2*I + 1)
    m_range = np.arange(I, -I-1, -1)
    m, mp = np.meshgrid(m_range, m_range)
    delp = (mp == m + 1).astype(np.int)
    delm = (mp == m - 1).astype(np.int)
    del0 = (mp == m).astype(np.int)
    Cp = np.sqrt((I - m)*(I + m + 1))
    Cm = np.sqrt((I + m)*(I - m + 1))

    Ip = Cp*delp
    Im = Cm*delm
    Ix = (Ip + Im)/2
    Iy = (Ip - Im)/2j
    Iz = m*del0

    return Ix, Iy, Iz, Ip, Im

if __name__ == "__main__":
    I = 3/2
    Ix1, Iy1, Iz1, Ip1, Im1 = get_spin_matrices(I)
    Ix2, Iy2, Iz2, Ip2, Im2 = nmrlib.core.get_spin_matrices(I)
