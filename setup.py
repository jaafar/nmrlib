import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="nmrlib",
    version="0.0.1",
    author="Jaafar Ansari",
    author_email="jaafaransari@gmail.com",
    description="Library for simulating NMR phenomena",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/jaafar/nmrlib",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.7',
    install_requires=['numpy>=1.18.5',
                      'pandas>=1.0.5'],
    include_package_data=True
)
